Title: Degafamisons Internet
Date: 2020-11-20 15:30
Modified: 2020-11-20 15:45
Category: vie-privée
Tags: Framasoft, Gafam
Slug: degafamisons-Internet
Summary: Les géants du web centralisent nos vies numériques en échange de leurs services

Les géants du web centralisent nos vies numériques en échange de leurs services

## Quels sont les enjeux ?

Ces dernières années ont vu se généraliser une **concentration des acteurs d’Internet** (Youtube appartient à Google, WhatsApp à Facebook, Skype à Microsoft, etc.). Cette centralisation est nuisible, non seulement parce qu’elle freine l’innovation, mais surtout parce qu’elle entraîne une perte de liberté pour les visiteurs. **Les utilisateurs de ces derniers services ne contrôlent plus leur vie numérique** : leurs comportements sont disséqués en permanence afin de mieux être ciblés par la publicité, et leurs données - pourtant privées (sites visités, mails échangés, vidéos regardées, etc.) - peuvent être analysées par des services gouvernementaux.

Retrouvez la [source](https://degooglisons-internet.org/fr) sur le site de Framasoft...