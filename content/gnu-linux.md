Title: GNU/Linux
Date: 2020-11-20 15:00
Modified: 2020-11-20 15:35
Category: logiciel-libre
Tags: April, GNU, Linux
Slug: gnu-linux
Summary: GNU est un jeu de mots récursif signifiant GNU's Not Unix

Le projet GNU a commencé de zéro, et ses premiers développements avaient pour but de créer un environnement de développement utilisable. Les premiers efforts se sont donc très logiquement portés sur l'élaboration d'un compilateur (gcc), d'un éditeur de textes (Emacs), ainsi que d'un déboggeur (gdb). Un noyau (Hurd) était aussi prévu, mais son élaboration prend plus de temps que prévu.

En 1991, Linus Torvalds commence à écrire son noyau, qui connaîtra plus tard le succès que l'on sait. Le projet GNU l'adopte en attendant que Hurd soit opérationnel, et le noyau Linux devient alors composant du système GNU/Linux.

Le système GNU/Linux est réputé pour sa fiabilité et sa robustesse. Ceci est dû en partie à la liberté des logiciels qui le composent, l'accès aux sources permettant de corriger très facilement et rapidement une erreur de programmation.

Le système GNU/Linux est fortement POSIX (POSIX est un ensemble de normes définissant un système UNIX idéal), ce qui le rend très similaire à la plupart des UNIX propriétaires existants. Le noyau Linux est multi-tâches, multi-utilisateurs, et intègre la plupart des technologies les plus récentes (SMP, clustering, RAID, ...).

GNU/Linux est aujourd'hui utilisé aussi bien par les entreprises que par les utilisateurs finaux.

De nombreuses applications sont disponibles pour l'utilisateur non informaticien. Par exemple, l'environnement graphique Gnome permet d'utiliser son ordinateur sans utiliser la ligne de commande. Gimp est un logiciel de traitement d'images très puissant. Gnumeric est un tableur intégré au projet Gnome. Ce ne sont que des exemples, de nombreuses applications peuvent rendre d'immenses services à l'utilisateur. La sécurité et la fiabilité de ce système sont un argument supplémentaire pour l'utiliser (les virus n'existent pas). Plusieurs distributions existent, et l'installation du système GNU/Linux est désormais facile.

Pour l'entreprise, les champs d'application sont vastes. La robustesse du système couplée à l'excellence des applications orientées réseau (Apache par exemple) en fait un système de choix pour les serveurs (le couple GNU/Linux et Apache est le plus utilisé au monde en matière de serveurs web). Les outils de développement (en particuliers les outils GNU : Emacs, gcc, cvs, ...) font de GNU/Linux une plate-forme de développement puissante et agréable à utiliser. En dehors de ces considérations techniques, l'utilisation de logiciels libres affranchit de plus l'entreprise de la main-mise d'un éditeur.

Retrouvez la [source](https://www.april.org/logiciels-libres-une-introduction#ToC3) sur le site de l'April...