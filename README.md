Voici un projet montrant la procédure pour propulser un blog à l'aide du logiciel [Pelican][pelican] et du thème [Alchemy][pelican-alchemy].

# Installation et configuration
> La procédure qui suit suppose que vous êtes dans un environnement GNU/Linux et que vous avez déjà installé l'environnement Pelican (par exemple sous Debian via la commande `apt install pelican `).

Nous commençons par créer un répertoire de travail :
```
mkdir -p ~/prog/Pelican/blog_pelican
cd ~/prog/Pelican/blog_pelican
```

## Configuration de pelican
Nous lançons ensuite la procédure de configuration basique avec la commande `pelican-quickstart` :
```
pelican-quickstart
Welcome to pelican-quickstart v4.0.1.

This script will help you create a new Pelican-based website.

Please answer the following questions so this script can generate the files
needed by Pelican.


> Where do you want to create your new web site? [.] 
> What will be the title of this web site? Demo Blog
> Who will be the author of this web site? moulinux
> What will be the default language of this web site? [fr] 
> Do you want to specify a URL prefix? e.g., https://example.com   (Y/n) n
> Do you want to enable article pagination? (Y/n) n
> What is your time zone? [Europe/Paris] 
> Do you want to generate a tasks.py/Makefile to automate generation and publishing? (Y/n) 
> Do you want to upload your website using FTP? (y/N) 
> Do you want to upload your website using SSH? (y/N) 
> Do you want to upload your website using Dropbox? (y/N) 
> Do you want to upload your website using S3? (y/N) 
> Do you want to upload your website using Rackspace Cloud Files? (y/N) 
> Do you want to upload your website using GitHub Pages? (y/N) 
Done. Your new project is available at $HOME/prog/Pelican/blog_pelican
```

## Configuration du thème
Nous créons un nouveau dossier et y clonons le thème :
```
mkdir themes
git clone https://github.com/nairobilug/pelican-alchemy themes/pelican-alchemy
```

Nous éditons alors le fichier `pelicanconf.py`, pour y ajouter la ligne suivante :
```
THEME = 'themes/pelican-alchemy/alchemy'
```

Nous pouvons maintenant lancer le serveur de développement :
```
make devserver
```

# Personnalisation du thème
Si nous souhaitons adopter le style *oldstyle*. Nous modifions le fichier `pelicanconf.py`, pour y ajouter la ligne suivante :
```
THEME_CSS_OVERRIDES = ['theme/css/oldstyle.css']
```

Nous pouvons également ajouter une image de profil, en créant un dossier `images` dans le dossier `content` et en y copiant une image. Par exemple, pour le fichier `gnulinux.png`, il nous faut ajouter la ligne suivante à la configuration de Pelican :
```
SITEIMAGE = '/images/gnulinux.png'
```

Nous pouvons également personnaliser les liens :
```
LINKS = (
    ('Portfolio enseignant', 'https://moulinux.frama.io/'),
    ('Accueil S2SIO', 'https://s2sio.frama.io/'),
    ('Accueil SLAM', 'https://slam.frama.io/'),
)
```

Il ne nous reste plus qu'à ajouter quelques articles dans le dossier `content`...

# Déploiement et intégration continue
Nous allons intégrer un fichier `.gitlab-ci.yml` à notre projet :
```
image: python:3-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican -s publishconf.py
  artifacts:
    paths:
    - public/
```
Ce code correspond aux instructions que nous donnons au moteur d’intégration continue de gitlab. Nous avons donc besoin d’intégrer un nouveau fichier texte qui va décrire les dépendances nécessaires pour construire notre projet python dans le conteneur de test qui sera utilisé sur le serveur. Nous éditons ainsi le fichier `requirements.txt` :
```
pelican
markdown
typogrify
```
Enfin, nous éditons le fichier de configuration `pelicanconf.py`, pour y ajouter la ligne de code suivante :
```
OUTPUT_PATH = 'public'
```
Le script de publication, qui est exécuté dans le conteneur doit générer les fichiers statiques dans le répertoire `public/`.
Dernière chose, notre site sera accessible en ajoutant `/AP/blog_pelican`, dans l’adresse **frama.io** associée à notre groupe, nous devons donc mettre à jour la variable `SITEURL` dans le fichier `publishconf.py` :
```
SITEURL = '/AP/blog_pelican'
```


[pelican]: https://blog.getpelican.com/
[pelican-alchemy]: https://framagit.org/julienosman/pelican-alchemy